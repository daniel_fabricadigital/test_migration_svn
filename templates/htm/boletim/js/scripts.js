	// Fun��o para valida��o do formul�rio
	function checkForm(myform) {
		var form = myform
		
		if(form.nome.value == "")
		{
			alert( "O campo 'Nome' n�o pode ser vazio. Preencha o campo 'Nome' e tente novamente." );
			form.nome.focus();
			return false;
		}
		if(form.email.value == "")
		{
			alert( "O campo 'E-mail' n�o pode ser vazio. Preencha o campo 'E-mail' e tente novamente." );
			form.email.focus();
			return false;
		}
		var reEmail = /^[\w!#$%&'*+\/=?^`{|}~-]+(\.[\w!#$%&'*+\/=?^`{|}~-]+)*@(([\w-]+\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
		if (!reEmail.test(form.email.value.replace(/^\s*/, "").replace(/\s*$/, ""))) 
		{
			alert( "O endere�o de e-mail n�o � v�lido." );
			form.email.focus();
			return false;
		}  
		return true;
	}
	
	
	