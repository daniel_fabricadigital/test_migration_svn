---------------------------------------------------------------
-- Formata um n�mero para dois d�gitos caso ele seja menor que 10
-- Recebe n�mero
--------------------------------------------------------------
function formatNumberTwoDitigts(n)
	local n = n
	if n and tonumber(n) < 10 then
		n = "0"..n
	end
	return n or nil
end
-- Recebe opcionalmente o ano em que come�aram as edi��es
-- O padr�o � 2009
-- TODO: Refatorar!
--------------------------------------------------------------
function showNewsletterEditions(n)
	local data_alvo = n or 20098
	local mes_corrente = month()
	local ano_corrente = year()
	local data_corrente = ano_corrente..mes_corrente
	
	-- Inserir comentario
	local strHtml = [[<span class="titulo_materia_secao">]]..ano_corrente..[[</span><br/>]]
	local url = [[http://]]..ServerName..startURL()..[[?tpl=boletimfd&UserActiveTemplate=boletim&NewletterParametersVisible=true]]
	local meses = {"Janeiro", "Fevereiro", "Mar�o","Abril", "Maio", "Junho", "Julho","Agosto", "Setembro", "Outubro","Novembro","Dezembro"}
	-- Inserir comentario
	
	while (tonumber(data_corrente) > tonumber(data_alvo)) do
		if mes_corrente == 1 then
		   mes_corrente = 12
		   ano_corrente = ano_corrente -1
		   strHtml = strHtml..[[<span class="titulo_materia_secao">]]..ano_corrente..[[</span><br/>]]
		end
		mes_corrente = mes_corrente - 1
   		data_corrente = ano_corrente..mes_corrente
   		strHtml = strHtml..[[<a href="]]..url..[[&data=]]..ano_corrente..formatNumberTwoDitigts(mes_corrente)..[[">]]..meses[mes_corrente]..[[</a><br/>]]
	end
   			
   
   	return strHtml
end
--------------------------------------------
function concat (t, concat)
	local s = {""}
	local concat = concat or ", "
	
	foreachi( t, function( idx, ID )
		if (idx > 1) then
			%s[1] = %s[1]..%concat..ID
		else
			%s[1] = %s[1]..ID
		end
	end)
	
	return s[1]
end
-----------------------------------------------
-- Pega o id de uma se��o a partir de um *tag*
-----------------------------------------------
function getSectionID(options)
       local options = options or {cgi.sid}
       local sectionTag = options.sectionTag

       sqlSectionIDByTag = [[
               SELECT F_SectionID FROM T_Section WHERE F_Tag like '%s'
       ]]
       connectDB()

       local sql =format(sqlSectionIDByTag, gsub(sectionTag, '%*', '%%'))

       DBExec(sql)

       local row = DBRow()

       return row and row.f_sectionid or 0
end