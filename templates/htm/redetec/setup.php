<?php

/**
 * setup.php
 *
 * Copyright (c) 1999-2003 The SquirrelMail Project Team
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 *
 * $Id: $
 */

if (!defined('SM_PATH'))  {
    define('SM_PATH','../../');
}

function squirrelmail_plugin_init_select_range(){
    global $squirrelmail_plugin_hooks;
  
    $squirrelmail_plugin_hooks['mailbox_index_before']['select_range'] = 'select_range_print_link';
    $squirrelmail_plugin_hooks['right_main_bottom']['select_range'] = 'select_range_print_link';
    $squirrelmail_plugin_hooks['optpage_loadhook_display']['select_range'] = 'select_range_options';
    $squirrelmail_plugin_hooks['mailbox_selectall_after']['select_range'] = 'select_range_return_link';
    $squirrelmail_plugin_hooks['addressbook_before_list']['select_range'] = 'select_range_abook';
    $squirrelmail_plugin_hooks['list_checkbox_extra_params']['select_range'] = 'select_range_checkbox_params';
}

function select_range_abook(){
    include_once( SM_PATH . 'plugins/select_range/functions.php' );
	 select_range_do_abook();
}

function select_range_options(){
    include_once( SM_PATH . 'plugins/select_range/functions.php' );
	 select_range_do_options();
}

function select_range_print_link(){
    include_once( SM_PATH . 'plugins/select_range/functions.php' );
	 select_range_do_print( 0, '' );
}

function select_range_return_link(){
    include_once( SM_PATH . 'plugins/select_range/functions.php' );
    return( select_range_do_print( 1, '&nbsp;&nbsp;' ) );
}

function select_range_checkbox_params($params){
    list($element_name,$always_show) = $params;
    include_once( SM_PATH . 'plugins/select_range/functions.php' );
    return( select_range_do_checkbox_params($element_name,$always_show) );
}

function select_range_version(){
    // untested code, hope it works...
	 $return_val;
    $handle = fopen( SM_PATH . 'plugins/select_range/version', 'r' );
	 if( isset( $handle ) ){
		  $contents = fread( $handle, filesize( SM_PATH . 'plugins/select_range/version' ) );
        // version should be second line of array
		  $return_val = rtrim( $contents[1] );
        fclose( $handle );
	 }

	 return( $return_val );
}

?>
